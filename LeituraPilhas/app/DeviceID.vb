Public Class Device
    Inherits System.Windows.Forms.Form

    Declare Function KernelIoControl Lib "CoreDll.dll" _
        (ByVal dwIoControlCode As Int32, _
        ByVal lpInBuf As IntPtr, _
        ByVal nInBufSize As Int32, _
        ByVal lpOutBuf() As Byte, _
        ByVal nOutBufSize As Int32, _
        ByRef lpBytesReturned As Int32) As Boolean

    Public Sub New()
        Me.Text = "DeviceID"
    End Sub

    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        MyBase.Dispose(disposing)
    End Sub

    Shared Sub Main()
        Application.Run(New Device)
    End Sub

    Private Shared METHOD_BUFFERED As Int32 = 0
    Private Shared FILE_ANY_ACCESS As Int32 = 0
    Private Shared FILE_DEVICE_HAL As Int32 = &H101

    Private Const ERROR_NOT_SUPPORTED As Int32 = &H32
    Private Const ERROR_INSUFFICIENT_BUFFER As Int32 = &H7A

    Private Shared IOCTL_HAL_GET_DEVICEID As Int32 = _
        (&H10000 * FILE_DEVICE_HAL) Or (&H4000 * FILE_ANY_ACCESS) _
        Or (&H4 * 21) Or METHOD_BUFFERED

    Public Shared Function GetDeviceID() As String
        Dim outbuff(19) As Byte
        Dim dwOutBytes As Int32
        Dim done As Boolean = False

        Dim nBuffSize As Int32 = outbuff.Length

        BitConverter.GetBytes(nBuffSize).CopyTo(outbuff, 0)
        dwOutBytes = 0

        While Not done
            If KernelIoControl(IOCTL_HAL_GET_DEVICEID, IntPtr.Zero, _
                0, outbuff, nBuffSize, dwOutBytes) Then
                done = True
            Else
                Dim errnum As Integer = System.Runtime.InteropServices.Marshal.GetLastWin32Error()
                Select Case errnum
                    Case ERROR_NOT_SUPPORTED
                        Throw New NotSupportedException( _
                            "IOCTL_HAL_GET_DEVICEID is not supported on this device", _
                            New ComponentModel.Win32Exception(errnum))

                    Case ERROR_INSUFFICIENT_BUFFER
                        nBuffSize = BitConverter.ToInt32(outbuff, 0)
                        outbuff = New Byte(nBuffSize) {}

                        BitConverter.GetBytes(nBuffSize).CopyTo(outbuff, 0)

                    Case Else
                        Throw New ComponentModel.Win32Exception(errnum, "Unexpected error")
                End Select
            End If
        End While

        Dim dwPresetIDOffset As Int32 = BitConverter.ToInt32(outbuff, &H4)
        Dim dwPresetIDSize As Int32 = BitConverter.ToInt32(outbuff, &H8)
        Dim dwPlatformIDOffset As Int32 = BitConverter.ToInt32(outbuff, &HC)
        Dim dwPlatformIDSize As Int32 = BitConverter.ToInt32(outbuff, &H10)
        Dim sb As New System.Text.StringBuilder
        Dim i As Integer

        For i = dwPresetIDOffset To (dwPresetIDOffset + dwPresetIDSize) - 1
            sb.Append(String.Format("{0:X2}", outbuff(i)))
        Next i

        sb.Append("-")

        For i = dwPlatformIDOffset To (dwPlatformIDOffset + dwPlatformIDSize) - 1
            sb.Append(String.Format("{0:X2}", outbuff(i)))
        Next i

        Return sb.ToString()
    End Function

    Public Function getDeviceName()
        Return System.Net.Dns.GetHostName()
    End Function
End Class
