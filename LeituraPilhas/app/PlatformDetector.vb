Public Class PlatformDetector

    Declare Function SystemParametersInfo Lib "Coredll.dll" _
         (ByVal uiAction As System.UInt32, _
         ByVal uiParam As System.UInt32, _
         ByVal pvParam As System.Text.StringBuilder, _
         ByVal fWinIni As System.UInt32) As Boolean


    Private Shared SPI_GETPLATFORMTYPE As System.UInt32 = 257

    Public Shared Function GetPlatform() As Platform
        Dim plat As Platform = Platform.Unknown
        Select Case System.Environment.OSVersion.Platform
            Case PlatformID.Win32NT
                plat = Platform.Win32NT
            Case PlatformID.Win32S
                plat = Platform.Win32S
            Case PlatformID.Win32Windows
                plat = Platform.Win32Windows
            Case PlatformID.WinCE
                plat = CheckWinCEPlatform()
        End Select

        Return plat

    End Function
    Shared Function CheckWinCEPlatform() As Platform
        Dim plat As Platform = Platform.WindowsCE
        Dim strbuild As New System.Text.StringBuilder(200)
        SystemParametersInfo(SPI_GETPLATFORMTYPE, 200, strbuild, 0)
        Dim str As String = strbuild.ToString()
        Select Case str
            Case "PocketPC"
                plat = Platform.PocketPC
            Case "SmartPhone"
                plat = Platform.Smartphone
        End Select
        Return plat
    End Function
End Class

Public Enum Platform
    PocketPC
    WindowsCE
    Smartphone
    Win32NT
    Win32S
    Win32Windows
    Unknown
End Enum
