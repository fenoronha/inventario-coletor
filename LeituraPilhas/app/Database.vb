Public Class Database


    Private Shared _instance As Database
    Private connectionString As String

    Public Shared Function getInstance() As Database
        If _instance Is Nothing Then
            _instance = New Database()
        End If
        Return _instance
    End Function


    Public Function createDatabase(ByVal connectionString As String)
        Dim engine As New Data.SqlServerCe.SqlCeEngine(connectionString)
        Try
            engine.CreateDatabase()
        Catch ex As Exception

        End Try
    End Function

    Public Function listarPilhasDatagrid() As Data.DataSet
        Dim selectCommad As Data.SqlServerCe.SqlCeCommand
        Dim adapter As Data.SqlServerCe.SqlCeDataAdapter
        Dim ds As Data.DataSet

        selectCommad = New Data.SqlServerCe.SqlCeCommand("Select * from IPACTBFIS", Database.getInstance.getLocalConnection())
        adapter = New Data.SqlServerCe.SqlCeDataAdapter(selectCommad)
        ds = New Data.DataSet()
        adapter.Fill(ds, "Pilha")

        Return ds
    End Function


    Public Function deletarPilharPorID(ByVal id As Integer)
        Dim localConnection As Data.SqlServerCe.SqlCeConnection = Database.getInstance.getLocalConnection()
        localConnection.Open()

        Dim deleteCommand As Data.SqlServerCe.SqlCeCommand
        deleteCommand = New Data.SqlServerCe.SqlCeCommand("DELETE FROM IPACTBFIS WHERE ID_IPACTBFIS = " + id.ToString(), localConnection)
        deleteCommand.ExecuteNonQuery()

        localConnection.Close()

    End Function

    Public Function dropTable()

        Dim localConnection As Data.SqlServerCe.SqlCeConnection = Database.getInstance.getLocalConnection()
        Dim sql = "drop table IPACTBFIS"
        Dim _dropTable As Data.SqlServerCe.SqlCeCommand


        localConnection.Open()
        Try
            _dropTable = New Data.SqlServerCe.SqlCeCommand(sql, localConnection)
            _dropTable.ExecuteNonQuery()
        Catch ex As Exception

        End Try
        localConnection.Open()
    End Function

    Public Function createTable()
        Dim localConnection As Data.SqlServerCe.SqlCeConnection = Database.getInstance.getLocalConnection()
        Dim _createTable As Data.SqlServerCe.SqlCeCommand
        Dim sql = ""
        localConnection.Open()

        sql = "CREATE TABLE IPACTBFIS ("
        sql = sql + " [EmpCod] smallint NOT NULL, "
        sql = sql + " [UndCod] smallint NOT NULL, "
        sql = sql + " [IpaCtbFisEndCod] nvarchar(10) NOT NULL, "
        sql = sql + " [IpaCtbFisPilCod] numeric NOT NULL, "
        sql = sql + " [IpaCtbFisUsr] nvarchar(15) NULL, "
        sql = sql + " [IpaCtbFisDat] datetime NULL, "
        sql = sql + " [IpaCtbFishor] datetime NULL, "
        sql = sql + " [IpaCtbFisQtd] smallint NULL, "
        sql = sql + " [ID_IPACTBFIS] INTEGER IDENTITY(1,1));"

        Try
            _createTable = New Data.SqlServerCe.SqlCeCommand(sql, localConnection)
            _createTable.ExecuteNonQuery()
        Catch ex As Exception

        End Try

        localConnection.Close()
    End Function

    Public Function getLocalConnection() As Data.SqlServerCe.SqlCeConnection
        Return New Data.SqlServerCe.SqlCeConnection(getLocalConnectionString())
    End Function

    Public Function getLocalConnectionString()
        Return "Data Source =\Inventario_teste.sdf;Password =sa;"
    End Function

    Public Function setConnetionString(ByVal server As String) As String
        connectionString = "Data Source=" + server + ";Initial Catalog=PRODUCAO; User Id=inventario; Password=inventario"
    End Function

    Public Function getConnectionString() As String
        Return connectionString
    End Function

    Public Function adicionaPilhaNaListaLocal(ByVal unidadeSelecionada As Integer, ByVal endereco As String, ByVal pilha As String, ByVal nomeDispositivo As String)

        Dim localConnection As Data.SqlServerCe.SqlCeConnection = Database.getInstance.getLocalConnection()
        Try
            Dim insertCommand As Data.SqlServerCe.SqlCeCommand
            localConnection.Open()

            Dim sql = "INSERT INTO IPACTBFIS (EmpCod, UndCod, IpaCtbFisEndCod ,IpaCtbFisPilCod , IpaCtbFisUsr , IpaCtbFisDat , IpaCtbFishor ,IpaCtbFisQtd) "
            sql = sql + " VALUES "
            sql = sql + "( 1 "
            sql = sql + ", " + unidadeSelecionada.ToString()
            sql = sql + ", '" + endereco + "'"
            sql = sql + ", '" + pilha + "'"
            sql = sql + ", '" + nomeDispositivo + "'"
            sql = sql + ", '" + Now + "'"
            sql = sql + ", '" + Now + "'"
            sql = sql + ", 13)"
            'Dim sql = "delete from IPACTBFIS"

            insertCommand = New Data.SqlServerCe.SqlCeCommand(sql, localConnection)

            insertCommand.ExecuteNonQuery()
            localConnection.Close()
        Catch ex As Exception
            MessageBox.Show(" problema ao inserir no banco de dados local")
        End Try
    End Function

    Public Function adicionaPilhaNaListaExterno(ByVal unidadeSelecionada As Integer, ByVal endereco As String, ByVal pilha As String, ByVal nomeDispositivo As String, ByVal data As Date, ByVal hora As Date)

        Dim cnn As Data.SqlClient.SqlConnection = New Data.SqlClient.SqlConnection(Database.getInstance().getConnectionString())
        Try
            Dim insertCommand As Data.SqlClient.SqlCommand
            cnn.Open()

            'Dim sql = "INSERT INTO IPACTBFIS (EmpCod, UndCod, IpaCtbFisEndCod ,IpaCtbFisPilCod , IpaCtbFisUsr , IpaCtbFisDat , IpaCtbFishor ,IpaCtbFisQtd) "


            Dim sql = "INSERT INTO IPACTBFIS (EmpCod, UndCod, IpaCtbFisEndCod ,IpaCtbFisPilCod , IpaCtbFisUsr , IpaCtbFisDat,	IpaCtbFishor, ipactbfisqtd) "
            sql = sql + " VALUES "
            sql = sql + "( 1 "
            sql = sql + ", " + unidadeSelecionada.ToString()
            sql = sql + ", '" + endereco + "'"
            sql = sql + ", '" + pilha + "'"
            sql = sql + ", '" + nomeDispositivo + "'"
            sql = sql + ", convert(datetime,'" + data.ToString() + "')"
            sql = sql + ", convert(datetime,'" + hora.ToString() + "')"
            sql = sql + ", 13)"
            'Dim sql = "delete from IPACTBFIS"

            insertCommand = New Data.SqlClient.SqlCommand(sql, cnn)

            insertCommand.ExecuteNonQuery()
            cnn.Close()
        Catch ex As Exception
            MessageBox.Show(" problema ao inserir no banco de dados externo")
        End Try
    End Function


End Class
