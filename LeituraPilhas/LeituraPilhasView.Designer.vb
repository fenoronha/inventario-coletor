<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class connectarServidorView
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.tabLeituraPilhas = New System.Windows.Forms.TabControl
        Me.tabConectar = New System.Windows.Forms.TabPage
        Me.cbxServidores = New System.Windows.Forms.ComboBox
        Me.lblDebug = New System.Windows.Forms.Label
        Me.lblServidor = New System.Windows.Forms.Label
        Me.tabLeitura = New System.Windows.Forms.TabPage
        Me.btnProcessar = New System.Windows.Forms.Button
        Me.btnOk = New System.Windows.Forms.Button
        Me.IPACTBFISBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.dgPilha = New System.Windows.Forms.DataGrid
        Me.InventarioDataSet = New LeituraPilhas.InventarioDataSet
        Me.txtEndereco = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtPilha = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.tabInformacoes = New System.Windows.Forms.TabPage
        Me.lblID = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.IPACTBFISTableAdapter = New LeituraPilhas.InventarioDataSetTableAdapters.IPACTBFISTableAdapter
        Me.btnCarregarEndereco = New System.Windows.Forms.Button
        Me.tabLeituraPilhas.SuspendLayout()
        Me.tabConectar.SuspendLayout()
        Me.tabLeitura.SuspendLayout()
        Me.tabInformacoes.SuspendLayout()
        Me.SuspendLayout()
        '
        'tabLeituraPilhas
        '
        Me.tabLeituraPilhas.Controls.Add(Me.tabConectar)
        Me.tabLeituraPilhas.Controls.Add(Me.tabLeitura)
        Me.tabLeituraPilhas.Controls.Add(Me.tabInformacoes)
        Me.tabLeituraPilhas.Location = New System.Drawing.Point(0, 0)
        Me.tabLeituraPilhas.Name = "tabLeituraPilhas"
        Me.tabLeituraPilhas.SelectedIndex = 0
        Me.tabLeituraPilhas.Size = New System.Drawing.Size(240, 291)
        Me.tabLeituraPilhas.TabIndex = 0
        '
        'tabConectar
        '
        Me.tabConectar.Controls.Add(Me.btnCarregarEndereco)
        Me.tabConectar.Controls.Add(Me.cbxServidores)
        Me.tabConectar.Controls.Add(Me.lblDebug)
        Me.tabConectar.Controls.Add(Me.lblServidor)
        Me.tabConectar.Location = New System.Drawing.Point(0, 0)
        Me.tabConectar.Name = "tabConectar"
        Me.tabConectar.Size = New System.Drawing.Size(240, 268)
        Me.tabConectar.Text = "Conectar"
        '
        'cbxServidores
        '
        Me.cbxServidores.Location = New System.Drawing.Point(68, 103)
        Me.cbxServidores.Name = "cbxServidores"
        Me.cbxServidores.Size = New System.Drawing.Size(165, 22)
        Me.cbxServidores.TabIndex = 4
        '
        'lblDebug
        '
        Me.lblDebug.Location = New System.Drawing.Point(7, 222)
        Me.lblDebug.Name = "lblDebug"
        Me.lblDebug.Size = New System.Drawing.Size(225, 20)
        Me.lblDebug.Text = "debug"
        '
        'lblServidor
        '
        Me.lblServidor.Location = New System.Drawing.Point(7, 105)
        Me.lblServidor.Name = "lblServidor"
        Me.lblServidor.Size = New System.Drawing.Size(58, 20)
        Me.lblServidor.Text = "Servidor :"
        '
        'tabLeitura
        '
        Me.tabLeitura.Controls.Add(Me.btnProcessar)
        Me.tabLeitura.Controls.Add(Me.btnOk)
        Me.tabLeitura.Controls.Add(Me.dgPilha)
        Me.tabLeitura.Controls.Add(Me.txtEndereco)
        Me.tabLeitura.Controls.Add(Me.Label2)
        Me.tabLeitura.Controls.Add(Me.txtPilha)
        Me.tabLeitura.Controls.Add(Me.Label1)
        Me.tabLeitura.Location = New System.Drawing.Point(0, 0)
        Me.tabLeitura.Name = "tabLeitura"
        Me.tabLeitura.Size = New System.Drawing.Size(240, 268)
        Me.tabLeitura.Text = "Leitura"
        '
        'btnProcessar
        '
        Me.btnProcessar.Location = New System.Drawing.Point(8, 245)
        Me.btnProcessar.Name = "btnProcessar"
        Me.btnProcessar.Size = New System.Drawing.Size(225, 20)
        Me.btnProcessar.TabIndex = 22
        Me.btnProcessar.Text = "Processar"
        '
        'btnOk
        '
        Me.btnOk.Location = New System.Drawing.Point(197, 35)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(36, 20)
        Me.btnOk.TabIndex = 19
        Me.btnOk.Text = "Ok"
        '
        'IPACTBFISBindingSource
        '
        Me.IPACTBFISBindingSource.DataMember = "IPACTBFIS"
        Me.IPACTBFISBindingSource.DataSource = Me.InventarioDataSet
        '
        'dgPilha
        '
        Me.dgPilha.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.dgPilha.DataSource = Me.IPACTBFISBindingSource
        Me.dgPilha.Location = New System.Drawing.Point(8, 69)
        Me.dgPilha.Name = "dgPilha"
        Me.dgPilha.Size = New System.Drawing.Size(225, 174)
        Me.dgPilha.TabIndex = 16
        '
        'InventarioDataSet
        '
        Me.InventarioDataSet.DataSetName = "InventarioDataSet"
        Me.InventarioDataSet.Prefix = ""
        Me.InventarioDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'txtEndereco
        '
        Me.txtEndereco.Location = New System.Drawing.Point(79, 8)
        Me.txtEndereco.Name = "txtEndereco"
        Me.txtEndereco.Size = New System.Drawing.Size(154, 21)
        Me.txtEndereco.TabIndex = 13
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(8, 35)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 20)
        Me.Label2.Text = "Pilha:"
        '
        'txtPilha
        '
        Me.txtPilha.Location = New System.Drawing.Point(78, 35)
        Me.txtPilha.Name = "txtPilha"
        Me.txtPilha.Size = New System.Drawing.Size(112, 21)
        Me.txtPilha.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(8, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 20)
        Me.Label1.Text = "Endere�o:"
        '
        'tabInformacoes
        '
        Me.tabInformacoes.Controls.Add(Me.lblID)
        Me.tabInformacoes.Controls.Add(Me.lblName)
        Me.tabInformacoes.Controls.Add(Me.Label4)
        Me.tabInformacoes.Controls.Add(Me.Label3)
        Me.tabInformacoes.Location = New System.Drawing.Point(0, 0)
        Me.tabInformacoes.Name = "tabInformacoes"
        Me.tabInformacoes.Size = New System.Drawing.Size(240, 268)
        Me.tabInformacoes.Text = "Informa��es"
        '
        'lblID
        '
        Me.lblID.Location = New System.Drawing.Point(29, 23)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(204, 202)
        '
        'lblName
        '
        Me.lblName.Location = New System.Drawing.Point(55, 3)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(178, 20)
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(8, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(27, 20)
        Me.Label4.Text = "ID:"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(8, 4)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 20)
        Me.Label3.Text = "Nome:"
        '
        'IPACTBFISTableAdapter
        '
        Me.IPACTBFISTableAdapter.ClearBeforeFill = True
        '
        'btnCarregarEndereco
        '
        Me.btnCarregarEndereco.Location = New System.Drawing.Point(8, 152)
        Me.btnCarregarEndereco.Name = "btnCarregarEndereco"
        Me.btnCarregarEndereco.Size = New System.Drawing.Size(224, 20)
        Me.btnCarregarEndereco.TabIndex = 7
        Me.btnCarregarEndereco.Text = "Carregar Endere�os"
        '
        'connectarServidorView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(240, 294)
        Me.Controls.Add(Me.tabLeituraPilhas)
        Me.Name = "connectarServidorView"
        Me.Text = "Leitura Pilhas"
        Me.tabLeituraPilhas.ResumeLayout(False)
        Me.tabConectar.ResumeLayout(False)
        Me.tabLeitura.ResumeLayout(False)
        Me.tabInformacoes.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tabLeituraPilhas As System.Windows.Forms.TabControl
    Friend WithEvents tabConectar As System.Windows.Forms.TabPage
    Friend WithEvents tabLeitura As System.Windows.Forms.TabPage
    Friend WithEvents lblServidor As System.Windows.Forms.Label
    Friend WithEvents lblDebug As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbxServidores As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtPilha As System.Windows.Forms.TextBox
    Friend WithEvents txtEndereco As System.Windows.Forms.TextBox
    Friend WithEvents tabInformacoes As System.Windows.Forms.TabPage
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dgPilha As System.Windows.Forms.DataGrid
    Friend WithEvents InventarioDataSet As LeituraPilhas.InventarioDataSet
    Friend WithEvents IPACTBFISBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents IPACTBFISTableAdapter As LeituraPilhas.InventarioDataSetTableAdapters.IPACTBFISTableAdapter
    Friend WithEvents btnOk As System.Windows.Forms.Button
    Friend WithEvents btnProcessar As System.Windows.Forms.Button
    Friend WithEvents btnCarregarEndereco As System.Windows.Forms.Button

End Class
