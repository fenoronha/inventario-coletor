Public Class Servidor

    Private _id As Integer
    Private _nome As String
    Private _servidor As String



    Property id() As Integer
        Get
            Return Me._id

        End Get
        Set(ByVal value As Integer)
            Me._id = value
        End Set
    End Property

    Property nome() As String
        Get
            Return Me._nome

        End Get
        Set(ByVal value As String)
            Me._nome = value

        End Set
    End Property

    Property servidor() As String
        Get
            Return Me._servidor

        End Get
        Set(ByVal value As String)
            Me._servidor = value
        End Set
    End Property


End Class
