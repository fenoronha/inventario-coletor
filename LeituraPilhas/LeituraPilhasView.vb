Public Class connectarServidorView

    Dim cnn As Data.SqlClient.SqlConnection
    Dim connetionString As String
    Dim platform As String
    Dim deviceId As String
    Dim deviceName As String
    Dim unidadeSelecionada As Integer
    Dim localConnection As Data.SqlServerCe.SqlCeConnection = Database.getInstance().getLocalConnection()


    Public Sub New() 'construtor
        Dim servidoresDataSource As Dictionary(Of String, String)
        InitializeComponent()

        Try
            platform = PlatformDetector.GetPlatform().ToString()
            deviceId = Device.GetDeviceID()
            deviceName = Device.getDeviceName()

            setDeviceInforamations(deviceName, deviceId)
        Catch ex As Exception
            MessageBox.Show("Problema o inicializar o aplicativo " + ex.Message.ToString())
        End Try

        servidoresDataSource = getDataSourceComboServidores()
        setDataSourceComboServidores(servidoresDataSource)

    End Sub

    Private Function setDeviceInforamations(ByVal deviceName As String, ByVal deviceID As String)
        lblName.Text = deviceName
        lblID.Text = deviceID
    End Function

    Private Function verificarConectarBancoExterno() As Boolean
        If Me.getUnidadeSelecionada() = 0 Then
            MessageBox.Show("Selecione um servidor...")
            cbxServidores.Focus()
            Return False
        End If

        cnn = New Data.SqlClient.SqlConnection(Database.getInstance().getConnectionString())
        Try
            cnn.Open()



            cnn.Close()
            Return True
        Catch ex As Exception
            MessageBox.Show("N�o foi possivel conectar no banco externo")
            Return False
        End Try
    End Function

    Private Sub servidoresChangeHandler(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxServidores.SelectedIndexChanged
        lblDebug.Text = cbxServidores.SelectedValue.ToString()
        Database.getInstance().setConnetionString(cbxServidores.SelectedValue.ToString())
        Me.setUnidadeSelecionada(cbxServidores.SelectedIndex)
    End Sub

    Private Function setUnidadeSelecionada(ByVal _unidadeSelecionada As Integer)
        unidadeSelecionada = _unidadeSelecionada
    End Function

    Private Function getUnidadeSelecionada() As Integer
        Return unidadeSelecionada
    End Function

    Private Sub setDataSourceComboServidores(ByVal dataSource As Dictionary(Of String, String))
        cbxServidores.DataSource = New BindingSource(dataSource, Nothing)
        cbxServidores.DisplayMember = "Value"
        cbxServidores.ValueMember = "Key"
        cbxServidores.SelectedIndex = 0
    End Sub

    Private Function getDataSourceComboServidores() As Dictionary(Of String, String)

        Dim servidoresDataSource As Dictionary(Of String, String) = New Dictionary(Of String, String)
        servidoresDataSource.Add("", "Selecione...")
        servidoresDataSource.Add("i22sqlja01", "Jacare�")
        servidoresDataSource.Add("SBRCBCCP0013", "Ca�apava")
        servidoresDataSource.Add("W2DS1B", "Barra Velha")

        Return servidoresDataSource
    End Function

    Private Sub connectarServidorView_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.IPACTBFISTableAdapter.Fill(Me.InventarioDataSet.IPACTBFIS)
    End Sub

    Private Sub okClickHandler(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click

        If validaCampos() = False Then
            Return
        End If

        If txtPilha.Text.Length() = 10 Then

            If getUnidadeSelecionada() = 0 Then
                MessageBox.Show("Selectione um servidor...")
                tabLeituraPilhas.SelectedIndex = 0
                cbxServidores.Focus()
                Return
            End If

            Database.getInstance().adicionaPilhaNaListaLocal(unidadeSelecionada, txtEndereco.Text, txtPilha.Text, deviceName)
            atualizaListaDataGrid()
            'MessageBox.Show("manda para o banco")
            txtPilha.Text = ""
            txtPilha.Focus()
        End If
    End Sub

    Private Function atualizaListaDataGrid()
        Try
            dgPilha.DataSource = Database.getInstance().listarPilhasDatagrid().Tables("Pilha").DefaultView

            Me.localConnection.Close()
        Catch ex As Exception
            MessageBox.Show("problema ao atualizar lista de pilhas")
        End Try
    End Function

    Private Sub processarClickHandler(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcessar.Click

        If getUnidadeSelecionada() = 0 Then
            MessageBox.Show("Selectione um servidor...")
            tabLeituraPilhas.SelectedIndex = 0
            cbxServidores.Focus()
            Return
        End If

        If verificarConectarBancoExterno() = False Then
            Return
        End If



        Dim dataSet As Data.DataSet = Database.getInstance().listarPilhasDatagrid()
        For Each linha As Data.DataRow In dataSet.Tables(0).Rows

            Dim p As Pilha = New Pilha()
            p.id = linha("ID_IPACTBFIS")
            p.empCod = linha("EmpCod")
            p.undCod = linha("UndCod")

            p.endereco = linha("IpaCtbFisEndCod")
            p.pilha = linha("IpaCtbFisPilCod")
            p.nomeColetor = linha("IpaCtbFisUsr")
            p.data = linha("IpaCtbFisDat")
            p.hora = linha("IpaCtbFishor")
            p.quantidade = linha("IpaCtbFisQtd")



            Database.getInstance().adicionaPilhaNaListaExterno(p.undCod, p.endereco, p.pilha, p.nomeColetor, p.data, p.hora)


            Database.getInstance().deletarPilharPorID(p.id)

            atualizaListaDataGrid()
        Next
        dataSet.Dispose()
    End Sub

    Private Function validaCampos() As Boolean
        If txtEndereco.Text.Length = 0 Then
            MessageBox.Show("Endere�o � obrigat�rio")
            Return False
        End If

        Return True

    End Function

    Private Sub textChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPilha.TextChanged

        If validaCampos() = False Then
            Return
        End If
        If txtPilha.Text.Length() = 10 Then

            If getUnidadeSelecionada() = 0 Then
                MessageBox.Show("Selectione um servidor...")
                tabLeituraPilhas.SelectedIndex = 0
                cbxServidores.Focus()
                Return
            End If

            Database.getInstance().adicionaPilhaNaListaLocal(unidadeSelecionada, txtEndereco.Text, txtPilha.Text, deviceName)
            atualizaListaDataGrid()
            'MessageBox.Show("manda para o banco")
            txtPilha.Text = ""
            txtPilha.Focus()
        End If

    End Sub

End Class
